How to use it:

Summary: 
The Stock-Exchange service is CRUD Spring Boot Rest API(/api/stocks) implemented using 'Design First' approach. 
Contract is generated using Swagger 2 and Swagger UI is implemented using "SwaggerConfig" class.
Used MongoDB for storing stock data. Use profiling for storing data for test and application/dev.
Initial Data will be loaded at start of application using CommandLineRunner
MockMvc is used for writing integration tests, test coverage is 100%, please find 'Coverage.png' in project.
Surefire report is generating for test case execution.

Prerequisite:
It is mandatory to install MongoDB local instance for running application. Start MongpDB instance locally.

Steps:
1. Clone the code from git using link "git clone https://bmanisha83@bitbucket.org/bmanisha83/stock-exchange.git"
2.1 (Mandatory- Installed mongoDB)Run mongodb with command "<INSTALL_PATH>/MongoDB\Server\4.0\bin\mongod.exe" --dbpath <PATH> 
2.2 Build it using "mvn clean install"
3. Run application using Spring Boot Application class "StockExchangeApplication"
4. For UI, can use freeMarker Template using url ("http://localhost:8081/api/stocks/") for listing stocks 
and you can also use swagger UI using url "http://localhost:8081/swagger-ui.html" on your favourite browser
or Postman for running restAPI .(note: run with application/json consumes and produce media type)
5. Could evaluate code coverage using IDE coverage plugin or can configure Surefire to generate html report.
5. Enjoy Exploring.