package com.stock.exchange;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.exchange.repository.StockExchangeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class demonstrates how to test a resource using Spring Boot @AutoConfigureMockMvc
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class StockResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private StockExchangeRepository stockExchangeRepository;

    @Before
    public void deleteAllBeforeTests() throws Exception {
        stockExchangeRepository.deleteAll();
    }

    @Test
    public void createStockShouldSucceed() throws Exception {
        // create stock
        MvcResult mvcResult = mockMvc.perform(post("/api/stocks").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockName\":\"Test_stock4\",\"currentPrice\":12.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}"))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void createStockShouldResultInBadRequest() throws Exception {
        // create not valid
        mockMvc.perform(post("/api/stocks").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockId\":\"Test123\",\"currentPrice\":null,\"currentPrice\":91.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}")).andExpect(
                status().isBadRequest());
    }

    @Test
    public void updateStockShouldSucceed() throws Exception {
        // create stock
        MvcResult mvcResult = mockMvc.perform(post("/api/stocks").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockName\":\"Test_stock4\",\"currentPrice\":12.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}"))
                .andExpect(status().isOk()).andReturn();

        String responseJson = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        // To put all of the JSON in a Map<String, Object>
        Map<String, Object> map = mapper.readValue(responseJson, Map.class);

        // Accessing the three target data elements
        String stockId = (String) map.get("stockId");
        // get stock by id
        String stockByIdUri = "/api/stocks/" + stockId;
        // update stock
        mockMvc.perform(put(stockByIdUri).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockName\":\"Test_stock4\",\"currentPrice\":91.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}"))
                .andExpect(status().isOk()).andReturn();
        // check updated or not
        mockMvc.perform(get(stockByIdUri)).andExpect(status().isOk()).andExpect(
                jsonPath("$.currentPrice").value("91.1"));
    }

    @Test
    public void updateStockShouldResultInBadRequest() throws Exception {
        // create stock
        MvcResult mvcResult = mockMvc.perform(post("/api/stocks").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockName\":\"Test_stock4\",\"currentPrice\":12.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}"))
                .andExpect(status().isOk()).andReturn();

        String responseJson = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        // To put all of the JSON in a Map<String, Object>
        Map<String, Object> map = mapper.readValue(responseJson, Map.class);

        // Accessing the three target data elements
        String stockId = (String) map.get("stockId");
        // get stock by id
        String stockByIdUri = "/api/stocks/" + stockId;
        // not valid put
        mockMvc.perform(put(stockByIdUri).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockId\":\"Test\",\"stockName\":\"Test_stock4\",\"currentPrice\":null,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}")).andExpect(
                status().isBadRequest());
    }

    @Test
    public void updateStockShouldResultInNotFoundRequest() throws Exception {
        // not found put
        mockMvc.perform(put("/api/stocks/Test").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockId\":\"Test\",\"stockName\":\"Test_stock4\",\"currentPrice\":91.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}")).andExpect(
                status().isNotFound());
    }

    @Test
    public void getStockShouldSucceedWhenStockExists() throws Exception {
        // create stock
        MvcResult mvcResult = mockMvc.perform(post("/api/stocks").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockName\":\"Test_stock4\",\"currentPrice\":12.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}"))
                .andExpect(status().isOk()).andReturn();

        String responseJson = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        // To put all of the JSON in a Map<String, Object>
        Map<String, Object> map = mapper.readValue(responseJson, Map.class);

        // Accessing the three target data elements
        String stockId = (String) map.get("stockId");

        // get stock by id
        String stockByIdUri = "/api/stocks/" + stockId;
        mockMvc.perform(get(stockByIdUri)).andExpect(status().isOk());
    }

    @Test
    public void getStocksShouldResultInNotFOundRequest() throws Exception {
        // get not found
        mockMvc.perform(get("/api/stocks/Test").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"stockId\":\"Test\",\"stockName\":\"Test_stock4\",\"currentPrice\":91.1,\"createdAt\":\"test\",\"lastUpdate\":\"test\"}")).andExpect(
                status().isNotFound());
    }

    @Test
    public void getAllStocksShouldSucceed() throws Exception {
        // getAllStocks
        String stockUri = "/api/stocks";
        mockMvc.perform(get(stockUri).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
    }
}

