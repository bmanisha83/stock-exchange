package com.stock.exchange.controller;

import com.stock.exchange.rest.api.StocksApi;
import com.stock.exchange.rest.model.StockRequest;
import com.stock.exchange.rest.model.StockResponse;
import com.stock.exchange.service.StockExchangeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.stock.exchange.mapper.StockExchangeMapper.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class StocksExchangeController implements StocksApi {

    private final StockExchangeService stockExchangeService;

    @Override
    public ResponseEntity<StockResponse> createStock(@Valid @RequestBody StockRequest stockRequest) {
        return ResponseEntity.ok(convertStockToStockResponse(stockExchangeService
                .createStock(convertStockRequestToStock(stockRequest))));
    }

    @Override
    public ResponseEntity<StockResponse> getStockById(@PathVariable("stockId") String stockId) {
        return ResponseEntity.ok(convertStockToStockResponse(stockExchangeService.getStock(stockId)));
    }

    @Override
    @GetMapping(value = "stocks")
    public ResponseEntity<List<StockResponse>> getStocks(@Valid @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                         @Valid @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {
        return ResponseEntity.ok(convertStockListToResponse(stockExchangeService.getStocks(page, size)));
    }

    @Override
    public ResponseEntity<Void> updateStock(@PathVariable("stockId") String stockId, @Valid @RequestBody StockRequest updateStock) {
        stockExchangeService.updateStock(stockId, convertStockRequestToStock(updateStock));
        return ResponseEntity.ok().build();
    }
}
