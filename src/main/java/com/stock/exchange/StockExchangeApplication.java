package com.stock.exchange;

import com.stock.exchange.mongodb.model.Stock;
import com.stock.exchange.service.StockExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication
@EnableMongoAuditing
public class StockExchangeApplication implements CommandLineRunner {

    @Autowired
    StockExchangeService stockExchangeService;

    public static void main(String[] args) {
        SpringApplication.run(StockExchangeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (stockExchangeService.getStockByName("DBOI").isEmpty()) {
            stockExchangeService.createStock(new Stock("DBOI", 12.12));
        }
        if (stockExchangeService.getStockByName("Payconiq").isEmpty()) {
            stockExchangeService.createStock(new Stock("Payconiq", 45.2));
        }

        if (stockExchangeService.getStockByName("IBM").isEmpty()) {
            stockExchangeService.createStock(new Stock("IBM", 90.09));
        }
    }

}
