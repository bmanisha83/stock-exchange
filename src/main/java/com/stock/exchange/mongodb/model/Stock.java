package com.stock.exchange.mongodb.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Document(collection = "stocks")
@CompoundIndexes({
        @CompoundIndex(
                unique = true,
                background = true,
                name = "stockName_unique_index",
                def = "{'stockName': 1}")
})
public class Stock {

    @Id
    private String stockId;

    private String stockName;

    private Double currentPrice;

    @CreatedDate
    private String createdAt;

    @LastModifiedDate
    private String lastUpdate;

    public Stock(String stockName, Double currentPrice) {
        this.stockName = stockName;
        this.currentPrice = currentPrice;
    }

}
