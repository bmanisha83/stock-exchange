package com.stock.exchange.mapper;

import com.stock.exchange.mongodb.model.Stock;
import com.stock.exchange.rest.model.StockRequest;
import com.stock.exchange.rest.model.StockResponse;

import java.util.List;
import java.util.stream.Collectors;

public class StockExchangeMapper {

    public static Stock convertStockRequestToStock(final StockRequest stockRequest) {
        return Stock.builder()
                .stockName(stockRequest.getStockName())
                .currentPrice(stockRequest.getCurrentPrice())
                .build();
    }

    public static List<StockResponse> convertStockListToResponse(List<Stock> stockList) {
        return stockList.stream()
                .map(StockExchangeMapper::convertStockToStockResponse)
                .collect(Collectors.toList());
    }

    public static StockResponse convertStockToStockResponse(final Stock stock) {
        return new StockResponse()
                .stockId(stock.getStockId())
                .stockName(stock.getStockName())
                .currentPrice(stock.getCurrentPrice())
                .createdAt(stock.getCreatedAt().toString())
                .lastUpdate(stock.getLastUpdate().toString());
    }

}