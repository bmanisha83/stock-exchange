package com.stock.exchange.repository;

import com.stock.exchange.mongodb.model.Stock;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockExchangeRepository extends MongoRepository<Stock, String> {
    // get stock by Name
    @Query("{ 'stockName' : ?0 }")
    List<Stock> getStockByName(String stockName);
}
