package com.stock.exchange.service;

import com.stock.exchange.mongodb.model.Stock;

import java.util.List;

public interface StockExchangeService {

    // save stock
    Stock createStock(Stock stock);

    // update stock operation
    Stock updateStock(String stockId, Stock stock);

    // get stock by Id
    Stock getStock(String id);

    // get all stocks
    List<Stock> getStocks(int page, int size);

    // get stock by Name
    List<Stock> getStockByName(String stockName);
}
