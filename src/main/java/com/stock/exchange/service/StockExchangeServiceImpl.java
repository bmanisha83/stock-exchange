package com.stock.exchange.service;

import com.stock.exchange.exception.StockNotFoundException;
import com.stock.exchange.mongodb.model.Stock;
import com.stock.exchange.repository.StockExchangeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Service("stockExchangeService")
public class StockExchangeServiceImpl implements StockExchangeService {

    @Autowired
    private final StockExchangeRepository stockExchangeRepository;

    @Override
    public Stock createStock(Stock stock) {
        log.info("Creating a stock {}", stock.getStockName());
        return stockExchangeRepository.insert(stock);
    }

    @Override
    public Stock getStock(String id) {
        log.info("Retrieving a stock id: {}", id);
        return stockExchangeRepository.findById(id)
                .orElseThrow(() -> new StockNotFoundException("Unable to find a stock id: " + id));
    }

    @Override
    public Stock updateStock(String stockId, Stock stock) {
        Stock savedStock = getStock(stockId);
        log.info("Updating stock {}", savedStock.getStockName());

        savedStock.setStockName(stock.getStockName());
        savedStock.setCurrentPrice(stock.getCurrentPrice());

        return stockExchangeRepository.save(savedStock);
    }

    @Override
    public List<Stock> getStocks(int page, int size) {
        log.info("Retrieving stocks");
        return stockExchangeRepository.findAll();
    }

    @Override
    public List<Stock> getStockByName(String stockName) {
        return stockExchangeRepository.getStockByName(stockName);
    }

}
