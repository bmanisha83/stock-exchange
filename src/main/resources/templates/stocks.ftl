<!DOCTYPE html>
<html lang="en">
<body>
<h1> List of stocks</h1>
<#if stocks?has_content>
    <ul>
        <#list stocks as s>
            <li>Id: ${s.getStockId()}, Name: ${s.getStockName()}</li>
        </#list>
    </ul>
<#else>
    No stocks added yet! Please use REST service to do so, for e.g. POST{
    "createdAt": "string",
    "currentPrice": 12.0,
    "lastUpdate": "string",
    "stockName": "Test"
    } using /api/stocks or the form below.
</#if>

</br></br>

<fieldset>
    <legend>Add Stock</legend>
    <form name="stocks" action="createStock" method="post">
        Stock Name: <input type="text" name="stockName"/> <br/>
        Stock Price: <input type="text" name="currentPrice"/> <br/>
        <input type="submit" value="Save"/>
    </form>
</fieldset>
</body>
</html>